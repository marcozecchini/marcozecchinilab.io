


## Education 

* [11/2019 - present day] **Ph.D. student in Data Science**, Department of Computer, Control, and Management Engineering of Sapienza University of Rome

* [09/2016 - 10/2018] **MSc in Computer Engineering** at Sapienza University of Rome.

* [09/2013 - 10/2016] **BSc in Computer Engineering** at Sapienza University of Rome.

* [09/2008 - 07/2013] **Scientific High School** at Liceo Scientifico Talete.

## Publications

### Journal publications:

* D. Pennino, M. Pizzonia, A. Vitaletti, M. Zecchini: Efficient Certification of Endpoint Control on Blockchain, IEEE Access, vol. 9, pp. 133309-133334, 2021. DOI: [10.1109/ACCESS.2021.3115343](https://doi.org/10.1109/ACCESS.2021.3115343).

* A. Cirillo, V. Dalena, A. Mauro, F. Mogavero, D. Pennino, M. Pizzonia, A. Vitaletti, M. Zecchini: Empowering citizens by a blockchain-Based Robinson list*, International Journal of Computers and Applications, 2021. DOI: [10.1080/1206212X.2021.1986245](https://doi.org/10.1080/1206212X.2021.1986245) 

* D. Pennino, M. Pizzonia, A. Vitaletti, M. Zecchini: Blockchain as IoT Economy Enabler: A Review of Architectural Aspects, Journal of Sensor and Actuator Networks, 2022. 11(2):20. DOI: [10.3390/jsan11020020](https://doi.org/10.3390/jsan11020020)

### Conference publications:
* F. Mogavero, I. Visconti, A. Vitaletti, M. Zecchini: The Blockchain Quadrilemma: When Also Computational Effectiveness Matters, BRAIN 2nd Workshop on Blockchain theoRy and ApplicatIoNS 2021 held in conjunction with IEEE Symposium on Computers and Communications (ISCC), Athens, Greece, September 5-8, 2021. [10.1109/ISCC53001.2021.9631511](https://doi.org/10.1109/ISCC53001.2021.9631511)

* M. Zecchini, A. A. Griesi, I. Chatzigiannakis, I. Mavrommati, D. Amaxilatis, O. Akrivopoulos: Using IoT Data-Driven Analysis of Water Consumption to support Design for Sustainable Behaviour during the COVID-19 Pandemic, 2021 6th South-East Europe Design Automation, Computer Engineering, Computer Networks and Social Media Conference (SEEDA-CECNSM), Preveza, Greece, September 24-26, 2021. DOI: [10.1109/SEEDA-CECNSM53056.2021.9566237](https://doi.org/10.1109/SEEDA-CECNSM53056.2021.9566237)

* M. Zecchini, A. A. Griesi, I. Chatzigiannakis, D. Amaxilatis, O. Akrivopoulos: Identifying Water Consumption Patterns in Education Buildings Before, During and After COVID-19 Lockdown Periods, 2021 IEEE International Conference on Smart Computing (SMARTCOMP), Irvine, CA, USA, August 23-27, 2021. DOI: [10.1109/SMARTCOMP52413.2021.00069](https://doi.org/10.1109/SMARTCOMP52413.2021.00069) 

* A. Cirillo, A. Mauro, D. Pennino, M. Pizzonia, A. Vitaletti, M. Zecchini: Decentralized Robinson List. CRYBLOCK 3rd Workshop on Cryptocurrencies and Blockchains for Distributed Systems held in conjunction with Conference on Mobile Computing and Networking (MobiCom), London, UK, September 21-25, 2020. DOI: [10.1145/3410699.3413790](https://doi.org/10.1145/3410699.3413790)

* D. Pennino, M. Pizzonia, A. Vitaletti, M. Zecchini: Binding of Endpoints to Identifiers by On-Chain Proofs. BRAIN 1st Workshop on Blockchain theoRy and ApplicatIoNS 2020 held in conjunction with IEEE Symposium on Computers and Communications (ISCC), Rennes, France, July 7-10, 2020. DOI: [10.1109/ISCC50000.2020.9219594](https://doi.org/10.1109/ISCC50000.2020.9219594)

* A. Bracciali, I. Chatzigiannakis, A. Vitaletti, M. Zecchini: Citizens Vote to Act: smart contracts for the management of water resources in smart cities. IEEE International Conference on Societal Automation (SA 2019), Krakow, Poland, September 4-6, 2019. DOI: [10.1109/SA47457.2019.8938093](https://doi.org/10.1109/SA47457.2019.8938093)

* M. Zecchini, A. Bracciali, I. Chatzigiannakis, A. Vitaletti: Smart Contract Design Patterns: a use case on water management.  International Workshop on Future Perspective of Decentralized APPlications (FPDAPP 2019), held in conjuction with the 25th International European Conference on Parallel and Distributed Computing (EUROPAR), Gottingen, Germany, August 26-30, 2019. DOI: [10.1007/978-3-030-48340-1_18](https://doi.org/10.1007/978-3-030-48340-1_18)

## Award

* In collaboration with Professor Vitaletti from Sapienza University, Albenzio Cirillo and Antonio Mauro from Fondazione Ugo Bordoni and Professor Pizzonia and Diego Pennino from University Roma 3, I participated as a finalist of [Future of Blockchain](https://www.futureofblockchain.co.uk/) international competition with the *"[Decentralized Robinson List](https://amauro.gitlab.io/fob_robinson_list/)"* project. We won the *Asset Creation and Management Platform Challenge* sponsored by [Algorand](https://www.algorand.com/)
