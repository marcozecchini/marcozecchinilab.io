---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Hi all,

I am Marco Zecchini. 


Currently, I am a Ph.D. student in Data Science at the [Department of Computer, Control and Management Engineering (DIAG)](https://www.dis.uniroma1.it/) of [Sapienza University of Rome](https://www.uniroma1.it/it/pagina-strutturale/home). 

My research interests are Internet of Things, Distributed Ledger Technologies, network protocols and distributed computing. 

My supervisors are [Andrea Vitaletti](https://andreavitaletti.github.io/docs/cv/#andrea-vitaletti) and [Ioannis Chatzigiannakis](http://ichatz.me/). 

## Education 

* [11/2019 - present day] **Ph.D. student in Data Science**, Department of Computer, Control, and Management Engineering of Sapienza University of Rome

* [09/2016 - 10/2018] **MSc in Computer Engineering** at Sapienza University of Rome.

* [09/2013 - 10/2016] **BSc in Computer Engineering** at Sapienza University of Rome.

* [09/2008 - 07/2013] **Scientific High School** at Liceo Scientifico Talete.

## PUBBLICATIONS

* M. Zecchini, A. Bracciali, I. Chatzigiannakis, A. Vitaletti (2019). "*On Refining Design Patterns for Smart Contracts: A use case on water management*", 2nd International Workshop on Future Perspective of Decentralized APPlications.

* A. Bracciali, I. Chatzigiannakis, A. Vitaletti, M. Zecchini (2019). "*Citizens Vote to Act: smart contracts for the management of water resources in smart cities*", 2019 First International Conference on Societal Automation (SA), 1-8.
