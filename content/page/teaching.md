---
title: Teaching
subtitle: Recent course activities
comments: false
---

## Advanced Information Systems Security and Blockchain (AISSBC) 2021/2022

Teaching Assistant at [AISSBC](https://sites.google.com/diag.uniroma1.it/aissab/home) course held by Professor Vitaletti and Professor Lazzeretti.

These are useful resources:

* [Openzeppelin class](https://github.com/marcozecchini/openzeppelin_tutorial)

## Internet of Things 2019/2020

Teaching Assistant at [Internet of Things 2020](http://ichatz.me/Site/InternetOfThings2020) course held by Professor Chatzigiannakis and Professor Vitaletti.

These are useful resources:

* [Marco Zecchini's Mbed profile](https://os.mbed.com/users/marcozecchini/) for seeing the examples we have seen during the classes 
 
* Low-Power Wide Area Network [Video Lesson 1](https://www.youtube.com/watch?v=sh2E4udX6XY&t=1s), [Video lesson 2](https://www.youtube.com/watch?v=RoUt0I5NGkk) and [Video Lesson 3](https://www.youtube.com/watch?v=Yrowf_Mzw_0)

* Blockchain and IoT lesson material on [GitHub](https://github.com/marcozecchini/iot-bc-handson)